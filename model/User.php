<?php

class User
{
    private $name;
    private $documentNumber;
    private $password;


    function __construct($name, $documentNumber, $password)
    {
        $this->name = $name;
        $this->documentNumber = $documentNumber;
        $this->password = $password;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }


    public function setDocumentNumber($documentNumber)
    {
        $this->documentNumber = $documentNumber;

        return $this;
    }


    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
}
