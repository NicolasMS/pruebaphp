<?php
require './model/User.php';

class UserController
{
    private $arrayUser = [];
    public function saveUser(User $user)
    {
        $this->arrayUser = [
            'name' => $user->getName(),
            'doc' => $user->getDocumentNumber(),
            'password' => $user->getPassword(),
        ];
        $this->getUser($user);
    }
    public function getUser(User $user)
    {
        return $user;
    }
    public function getArrayUsers()
    {
        return $this->arrayUser;
    }
    public function searchData(User $user)
    {
        if (
            $user->getName() == $this->arrayUser['name']
            && $user->getDocumentNumber() == $this->arrayUser['doc'] &&
            $user->getPassword() == $this->arrayUser['password']
        ) {
            return true;
        } else {
            return false;
        }
    }
    public function validateUser(User $user)
    {
        if (isset($_POST["nameUser"])  && isset($_POST["numberI"]) && isset($_POST["password"])) {
            if ($this->searchData($user)) {
                header("Location: welcome");
            } else {
                echo '<br>
                <div class = "alert-warning">
                Error al ingresar
                </div>';
            }
        }
    }
}
