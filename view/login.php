<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
    <header>
        <div class="container">
            <h1>VECTOR ITC</h1>
        </div>
    </header>
    <main>
        <div class="container d-flex align-content-center">
            <form method="post">
                <div class="form-group">
                    <label>Nombre de Usuario</label>
                    <input type="text" name="nameUser" class="form-control" placeholder="Email">
                </div>
                <br>
                <div class="form-group">
                    <label>Número de identificación</label>
                    <input type="text" name="numberIdentification" class="form-control" placeholder="N.Identificación">
                </div>
                <br>
                <div class="form-group">
                    <label>Contraseña</label>
                    <input type="password" name="password" class="form-control" placeholder="Contraseña">
                </div>
                <br>
                <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Ingresar</button>
                <?php
                    $userController = new UserController();
                    $user = $userController->validateUser(new User($_POST['nameUser'], $_POST['numberI'], $_POST['password']));
                ?>
            </form>
        </div>
    </main>
    <footer>

    </footer>
</body>

</html>